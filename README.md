[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/everyonecancontribute/everyonecancontribute.gitlab.io)

# GitLab pages project for #everyonecancontribute

Currently this is a work in progress Hugo page. Some features are missing, like:

* some minor CSS add like formatting the links inside the blog posts

## How to use hugo

If you want to write some text in hugo, just type `hugo server -D`. This will start hugo in server mode, which host a local web server on your machine (`http://localhost:1313`, checkout `hugo server -h` to change the default behaviour). Adding `-D` will make sure that also posts marked as draft will be build. You can mark a post as draft by adding `draft: true` to the [front matter](https://gohugo.io/content-management/front-matter) of your post. If your post have a publish date which is "in the future" relative to your local time you should also add `-F` otherwise it is not visible.

If you want to build the final version without host a web server just type `hugo`. The result is in the `public` directory of the root directory.

# How to add content

## Add your author bio

Add a new file under `data/author` with your name slug, eg `janedoe.yml`. The slug `janedoe` will be used later in the post to identify the author(s).

Currently the following attributes are mandatory:
* Name
* Web

Both are used within the posts, to show your name and your `Name` will be linked to the `Web` url.

Example content of the file will be:
```
Name: "Mario Kleinsasser"
Email: ""
Keybase: ""
Gitlab: ""
Github: ""
Linkedin: ""
Xing: ""
Twitter: ""
Web: "https://www.n0r1sk.com/mario"
Country: ""
Bio: ''
```

## Add a new post

The simplest way is to copy an existing post and to change the content accordingly. All content needs to be created under `content/post`. Please assign a date to the filename, it makes life easier if there will be many posts later.

Example content of a post file:
```
---
Title: "Everyone Can Contribute"
Subtitle: "... you too!"
Date: 2020-01-01
Description: ""
Aliases: []
Tags: []
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
Twitter:
  card: "summary"
  title: ""
  description: ""
  image: ""

---

Willkommen auf Seite des #everyonecancontribute Projekts!
```

### Add images to a post

Navigate into `content/post/images` and upload the image with the Web IDE while editing the blog post.

```
├── content
|   └── post
|     ├── your-post.md
|     ├── images
|     |  ├── your-image.png
```

You can reference the image with Markdown. Note that the Markdown preview with the Web IDE does not render the image, you need to use the local Hugo server to verify this.

```
![your image](/post/images/your-image.png)
```

In case you want to use the image as featured image, edit the front matter header above:

```
Featuredimage: "/post/images/your-image.png"
```

## Run the pipeline

After you've added content (by changing the master or merging a MR), the build pipeline needs to be run! To do so, **create a new tag** from **master** and the build pipeline will be started automatically.

