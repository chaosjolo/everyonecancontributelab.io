---
Title: "14. Kaeffchen (KubeCon Special): Read, analyse, search, share - includes EC2, SSH, CI/CD"
Date: 2020-08-19
Aliases: []
Tags: ["gitlab","culture","events","education","google","amazon","learn"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

![Live Forum analysis and search](/post/images/everyonecancontribute_kaeffchen_live_forum_community_help_search.png)

### General

- [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/27)
- Guests: Niclas Mietz, Nico Meisenzahl, Michael Friedrich, Mario Kleinsasser, Marcel Weinberg, Michael Aigner 
- Next ☕ chat `#15`: **2020-08-26** - [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/32) - GitLab Commit Kaeffchen - we will watch the [keynote](https://gitlabcommitvirtual2020.sched.com/event/dUWA/opening-keynote-the-power-of-gitlab) together! 


### Highlights

- How to read, analyse, search, and share experiences with a [live forum post](https://forum.gitlab.com/t/trouble-deploying-docker-container-to-ec2-via-ssh/41507/2?u=dnsmichi), also shared on [Twitter](https://twitter.com/dnsmichi/status/1296098136885284865)
- Amazon EC2 SSH keys in GitLab CI/CD related to [Kubernetes secrets](https://kubernetes.io/docs/concepts/configuration/secret/#creating-a-secret-manually): base64 encoding ftw :) 
- A taste of Austria and Germany: Dragee Keksi for Nico

### Recording

Enjoy the session!

{{< youtube TSp7gCk1H-s >}}

### Insights

- Nico's blog post: [Your Attackers Won't Be Happy — How GitLab Can Help You Secure Your Cloud-Native Applications!](https://about.gitlab.com/blog/2020/08/18/how-gitlab-can-help-you-secure-your-cloud-native-applications/)  

#### KubeCon Learnings

- [JaegerTracing, OpenTelemetry and Grafana](https://twitter.com/dnsmichi/status/1295726033346465797)
- [CNCF end user technology radar](https://twitter.com/olearycrew/status/1295721617037971456)
- [Falco - quick intro](https://twitter.com/dnsmichi/status/1295730513764810752)
- [Prometheus Q&A](https://twitter.com/dnsmichi/status/1295695780192321536)
- [OKD4](https://twitter.com/dnsmichi/status/1295733831606075392) - RedHat's Community OpenShift edition
- [KubeCon - GitLab features](https://twitter.com/dnsmichi/status/1296051268448915456)
- [Dockerhub rate limiting idea](https://twitter.com/alexellisuk/status/1295783066489835523)
- [Everyone can contribute](https://twitter.com/dnsmichi/status/1295720228777873410)

#### A taste of Austria and Germany

- Michael's list: https://gitlab.com/dnsmichi/atasteofaustria 
- Coffee recommended by Nico: https://kolla-kaffee.de/produkt/k1/ 
- MyAcker: https://www.myacker.com/de/ 

### News

- [CI/CD Pipeline monitoring with Prometheus](https://twitter.com/dnsmichi/status/1295414653254873088)
- [Thanos query frontend](https://twitter.com/bwplotka/status/1295423237749383173), blazingly fast.
- GitLab 13.3 on Aug 22 - Saturday, [Draft release blog post](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/58559)
  - Matrix builds
  - Package registry and SAST security analyzers for everyone
  - Coverage guided fuzz testing 
  - [Squash Commits when merging required](https://twitter.com/teawaterwire/status/1294184625829548033)
  - [CI/CD linter improvements](https://twitter.com/j4yav/status/1295685735027286016)
  - [Michael's Release Evangelism](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/3279)
- [InfluxDB OSS 2.0 to GA](https://twitter.com/thallinflux/status/1293635031513141249)
- [Nomad 0.12.2: Accessibility for charts thread](https://twitter.com/dingoeatingfuzz/status/1293608243374116865?s=12)
- [Kubernetes Monitoring with Prometheus](https://twitter.com/dnsmichi/status/1293878824808189952) 
- [Tracing Syscalls and security events with Tracee using eBPF](https://twitter.com/lizrice/status/1293625558564253696?s=12)
- [Announcing New Collections on HashiCorp Learn](https://www.hashicorp.com/blog/announcing-new-collections-on-hashicorp-learn/)
- [QuestDB Helm chart: InfluxDB port patch](https://twitter.com/solidnerd/status/1294626575191875584)
- [Ansible 2.9.12 breaking changes](https://twitter.com/Arrfab/status/1295738643047485442) 
- [CI rules for triggering a pipeline from the scheduler](https://twitter.com/snim2/status/1296006789285249027)
- [Hetzner service discovery for Prometheus](https://twitter.com/dnsmichi/status/1295823477279272961)
- [Kali 2020.3 release](https://twitter.com/kalilinux/status/1295788918399471616)
- [Rest ethics, CNN article](https://twitter.com/darrenmurph/status/1295767093820915715)







