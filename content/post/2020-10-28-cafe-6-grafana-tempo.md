---
Title: "6. Cafe: Grafana Tempo"
Date: 2020-10-28
Aliases: []
Tags: ["grafana","tempo","tracing","monitoring","observability"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### Highlights

We started with Goutham's blog post [Tempo: A game of trade-offs](https://gouthamve.dev/tempo-a-game-of-trade-offs/) to get a better idea, with then going a bit into the basics of Tracing and Spans, reviving a presentation on [OpenTracing and OpenMetrics](https://docs.google.com/presentation/d/1MAVFeSsTNVWC9wPGOlg83wh8GFtR9hPbdVHuumtgOWA/edit) and looking into the [OpenTracing Specification](https://opentracing.io/specification/). 

The blog post on [Metrics, Tracing, Logging](https://peter.bourgon.org/blog/2017/02/21/metrics-tracing-and-logging.html) still sparks many ideas and we put that into context with Grafana Tempo, Loki, Prometheus and more.

The hands on with [Grafana Tempo Getting Started](https://grafana.com/docs/tempo/latest/getting-started/) turned into trying out the [docker-compose example](https://github.com/grafana/tempo/tree/master/example#docker-compose) as this has a demo app for generating traces inside. In the end we had beautiful traces inside Grafana - try it out yourself, and share on social! 

### Recording

Enjoy the session! 🦊 

{{< youtube rOpNoFPXPI0 >}}


#### Bookmarks 

- https://gouthamve.dev/tempo-a-game-of-trade-offs/ 
- https://peter.bourgon.org/blog/2017/02/21/metrics-tracing-and-logging.html
- https://grafana.com/docs/tempo/latest/getting-started/
- https://github.com/grafana/tempo/tree/master/example#docker-compose


### Insights

- [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/55)
- Guests: [Niclas Mietz](https://twitter.com/solidnerd), [Michael Friedrich](https://twitter.com/dnsmichi), [Michael Aigner](https://twitter.com/solidnerd), [Marcel Weinberg](https://twitter.com/winem_), [Alejandro Michell](https://www.linkedin.com/in/alejandromichell/), [Nico Meisenzahl](https://twitter.com/nmeisenzahl)
- [Next ☕ chat `#7`](https://gitlab.com/everyonecancontribute/general/-/issues/60)





