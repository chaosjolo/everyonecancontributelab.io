---
Title: "13. Cafe: Auto-scaling GitLab runners in Hetzner Cloud"
Date: 2021-01-20
Aliases: []
Tags: ["gitlab","cicd","hetzner","cloud","autoscale"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### Highlights

When Patricia tweeted on a Monday about compiling Chromium and asked for self-hosting and cloud environments, we quickly [jumped](https://twitter.com/solidnerd/status/1351249257085476864) into trying it out on Wednesday with GitLab and auto-scaling the Runners in Hetzner Cloud. Niclas Mietz on the keyboard, Max Rosin stepping through his great blog post.

- [Documentation](https://docs.gitlab.com/runner/configuration/autoscale.html)
- [Blog post from Max Rosin](https://fotoallerlei.com/blog/post/2020/autoscaling-gitlab-runners-on-hetzner-cloud/post)
- [CI autoscaling research](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27061)
- [docker-machine direction in GitLab](https://gitlab.com/groups/gitlab-org/-/epics/2502)
- [Hetzner Cloud curated lists](https://github.com/hetznercloud/awesome-hcloud)

Workaround for Docker problem. runners.machine - MachineOptions

`"engine-install-url=https://releases.rancher.com/install-docker/19.03.9.sh"`

### Recording

Enjoy the session! 🦊 

{{< youtube isKaBJ4VT24 >}}

