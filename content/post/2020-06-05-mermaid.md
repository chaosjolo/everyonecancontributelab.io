---
Title: "GitLab Flavored Markdown: Mermaid"
Date: 2020-06-05
Aliases: []
Tags: []
Categories: ["Community", "UX", "markdown"]
Authors: ["tonka3000"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

GitLab Flavored Markdown is a really powerful language. You can easily write text in your GitLab issue or merge request. But there are some features which you maybe don't know about which can make your experience with GitLab much better.

> NOTE: If you want to find out more about GitLab Flavored Markdown, just click on the `Markdown` link in the bottom left corner of every markdown text field in GitLab or checkout the documentation of [GitLab Flavored Markdown](https://docs.gitlab.com/ee/user/markdown.html).

## mermaid
[mermaid](https://mermaid-js.github.io/mermaid) is an awesome library to create diagrams and flowcharts. It's designed very similar as markdown itself (in spirit) and is really easy to use. You can just write your mermaid code and generate beautiful diagrams out of it.

```
sequenceDiagram
  Michael->>+Mario: Hello mario, how are you?
  Mario->>+Michael: Fine
```

will be rendered to

{{< mermaid >}}
  sequenceDiagram
  Michael->>+Mario: Hello mario, how are you?
  Mario->>+Michael: Fine
{{< /mermaid >}}

Checkout the [mermaid live editor](https://mermaid-js.github.io/mermaid-live-editor) to play around with the different types of diagrams.

## mermaid in GitLab
[mermaid](https://mermaid-js.github.io/mermaid) is directly supported in GitLab, out of the box, anywhere you can type markdown.

This includes

* issue description
* issue comments
* merge request description
* merge request comments
* markdown files in a repo with f.e. a `.md` extension

You can use normal [markdown code fences](https://www.markdownguide.org/extended-syntax/#fenced-code-blocks) to code your diagram. Our mermaid code example from above would be used in GitLab Flavored Markdown with the following code ...

~~~

```mermaid
  sequenceDiagram
  Michael->>+Mario: Hello mario, how are you?
  Mario->>+Michael: Fine
```

~~~

and will be rendered to

{{< mermaid >}}
  sequenceDiagram
  Michael->>+Mario: Hello mario, how are you?
  Mario->>+Michael: Fine
{{< /mermaid >}}

In [GitLab](https://gitlab.com) `13.0` mermaid `8.4.8` is bundled with GitLab itself, so most features of mermaid are available.

### But why should you use mermaid in GitLab?
Do you know the phrase "A picture is worth a thousand words". In many situations a picture of something transport your idea/message much more then plain text. But creating pictures always require external applications and afterwards you need to copy that picture back to GitLab. When you need a change you hopefully have the original file with all your picture layers like in Photoshop. In short a terrible workflow. Here is where mermaid comes into play. Just write your "simple" mermaid code and you get beautiful diagrams and flowcharts and if you need to change something just edit your f.e. issue like regular text.

Pros:
* easy to write/create (you don't need to be a graphic designer)
* easy to edit
* create vector graphics instead of pixel based images
* often much smaller in size then pixel based images

Cons:
* you need to learn mermaid syntax
* really complex diagrams maybe not possible (but sometimes you can just split your diagram into multiple ones to overcome this issue)

### GitLab mermaid examples
You can use one of the following mermaid types in your next GitLab issue/merge request/comment.

* Flow chart
  ~~~
  ```mermaid
  graph TD
  A[Christmas] -->|Get money| B(Go shopping)
  B --> C{Let me think}
  C -->|One| D[Laptop]
  C -->|Two| E[iPhone]
  C -->|Three| F[fa:fa-car Car]
  ```
  ~~~
  {{< mermaid >}}
  graph TD
  A[Christmas] -->|Get money| B(Go shopping)
  B --> C{Let me think}
  C -->|One| D[Laptop]
  C -->|Two| E[iPhone]
  C -->|Three| F[fa:fa-car Car]
  {{< /mermaid >}}
* Sequence Diagram
  ~~~
  ```mermaid
  sequenceDiagram
    Michael->>+Mario: Hello mario, how are you?
    Mario->>+Michael: Fine
  ```
  ~~~
  {{< mermaid >}}
  sequenceDiagram
  Michael->>+Mario: Hello mario, how are you?
  Mario->>+Michael: Fine
  {{< /mermaid >}}
* Class diagram
  ~~~
  ```mermaid
  classDiagram
    Animal <|-- Duck
    Animal <|-- Fish
    Animal <|-- Zebra
    Animal : +int age
    Animal : +String gender
    Animal: +isMammal()
    Animal: +mate()
    class Duck{
      +String beakColor
      +swim()
      +quack()
    }
    class Fish{
      -int sizeInFeet
      -canEat()
    }
    class Zebra{
      +bool is_wild
      +run()
    }
  ```
  ~~~
  {{< mermaid >}}
  classDiagram
    Animal <|-- Duck
    Animal <|-- Fish
    Animal <|-- Zebra
    Animal : +int age
    Animal : +String gender
    Animal: +isMammal()
    Animal: +mate()
    class Duck{
      +String beakColor
      +swim()
      +quack()
    }
    class Fish{
      -int sizeInFeet
      -canEat()
    }
    class Zebra{
      +bool is_wild
      +run()
    }
  {{< /mermaid >}}
* State diagram
  ~~~
  ```mermaid
  stateDiagram
  [*] --> Still
  Still --> [*]

  Still --> Moving
  Moving --> Still
  Moving --> Crash
  Crash --> [*]
  ```
  ~~~
  {{< mermaid >}}
  stateDiagram
  [*] --> Still
  Still --> [*]

  Still --> Moving
  Moving --> Still
  Moving --> Crash
  Crash --> [*]
  {{< /mermaid >}}
* Gantt chart
  ~~~
  ```mermaid
  gantt
  title A Gantt Diagram
  dateFormat  YYYY-MM-DD
  section Section
  A task           :a1, 2014-01-01, 30d
  Another task     :after a1  , 20d
  section Another
  Task in sec      :2014-01-12  , 12d
  another task      : 24d
  ```
  ~~~
  {{< mermaid >}}
  gantt
  title A Gantt Diagram
  dateFormat  YYYY-MM-DD
  section Section
  A task           :a1, 2014-01-01, 30d
  Another task     :after a1  , 20d
  section Another
  Task in sec      :2014-01-12  , 12d
  another task      : 24d
  {{< /mermaid >}}
* Pie chart
  ~~~
  ```mermaid
  pie title Pets adopted by volunteers
  "Dogs" : 386
  "Cats" : 85
  "Rats" : 15
  ```
  ~~~
  {{< mermaid >}}
  pie title Pets adopted by volunteers
  "Dogs" : 386
  "Cats" : 85
  "Rats" : 15
  {{< /mermaid >}}
* Git graph ⚠️ experimental ⚠️
  ~~~
  ```mermaid
  gitGraph:
  options
  {
      "nodeSpacing": 70,
      "nodeRadius": 10
  }
  end
  commit
  branch newbranch
  checkout newbranch
  commit
  commit
  checkout master
  commit
  commit
  merge newbranch
  ```
  ~~~
  {{< mermaid >}}
  gitGraph:
options
{
    "nodeSpacing": 150,
    "nodeRadius": 10
}
end
commit
branch newbranch
checkout newbranch
commit
commit
checkout master
commit
commit
merge newbranch
  {{< /mermaid >}}
* ER diagram ⚠️ experimental ⚠️
  ~~~
  ```mermaid
  erDiagram
    CUSTOMER }|..|{ DELIVERY-ADDRESS : has
    CUSTOMER ||--o{ ORDER : places
    CUSTOMER ||--o{ INVOICE : "liable for"
    DELIVERY-ADDRESS ||--o{ ORDER : receives
    INVOICE ||--|{ ORDER : covers
    ORDER ||--|{ ORDER-ITEM : includes
    PRODUCT-CATEGORY ||--|{ PRODUCT : contains
    PRODUCT ||--o{ ORDER-ITEM : "ordered in"
  ```
  ~~~
  {{< mermaid >}}
  erDiagram
    CUSTOMER }|..|{ DELIVERY-ADDRESS : has
    CUSTOMER ||--o{ ORDER : places
    CUSTOMER ||--o{ INVOICE : "liable for"
    DELIVERY-ADDRESS ||--o{ ORDER : receives
    INVOICE ||--|{ ORDER : covers
    ORDER ||--|{ ORDER-ITEM : includes
    PRODUCT-CATEGORY ||--|{ PRODUCT : contains
    PRODUCT ||--o{ ORDER-ITEM : "ordered in"
  {{< /mermaid >}}
