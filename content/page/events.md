---
Title: "Events"
Date: 2020-06-20
Authors: ["dnsmichi"]
mermaid: true
---

## GitLab Commit 2020
Yearly [GitLab Commit](https://about.gitlab.com/events/commit/) conference on Aug 26th. This year completely virtual.

## Alle Events von #everyonecancontribute!

Website updates, review app for changes, Handbook first & collaboration and documentation (Static Site editor), Paessler webcast, protecting CI configuration & DevSecOps culture.